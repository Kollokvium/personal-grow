import UIKit

// 👀 За кем наблюдают
protocol Observable {
    func add(observer: Observer)
    func remove(observer: Observer)
    func notifyObservers()
}

// 👀 Кто наблюдает
protocol Observer {
    var id: String { get set }
    var basketItems: [String] { get set }
    
    func update(value: [String])
}

final class Basket: Observable {
    
    var basketItems = [String]() {
        didSet {
            notifyObservers()
        }
    }
    
    private var observers: [Observer] = []
    
    func add(observer: Observer) {
        observers.append(observer)
    }
    
    func remove(observer: Observer) {
        guard let index = observers
            .enumerated()
            .first(where: { $0.element.id == observer.id })?
            .offset else { return }
        observers.remove(at: index)
    }
    
    internal func notifyObservers() {
        observers.forEach({ $0.update(value: basketItems) })
    }
}

final class BasketWatcher: Observer {
    var id: String = "BasketWatcher"
    
    var basketItems: [String] = []
    
    func update(value: [String]) {
        print("items in basket was changed to: \(value)")
    }
}

let basket = Basket()
let basketWatcher = BasketWatcher()

// Seting Up initial basket screen state
basket.add(observer: basketWatcher)

// Setting Up user interaction with basket
basket.basketItems.append("Cola")
basket.basketItems.append("Fanta")
basket.basketItems.append("Sprite")
basket.basketItems.append("Beef")
basket.basketItems.append("Wine")

// Remove observing (for some weird reason)
basket.remove(observer: basketWatcher)
