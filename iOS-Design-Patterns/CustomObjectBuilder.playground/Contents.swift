import UIKit

class SpecificViewBuilder {
    func build() -> UIView {
        let customizedView = UIView()
        customizedView.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        customizedView.backgroundColor = .yellow
        customizedView.layer.cornerRadius = 10
        customizedView.clipsToBounds = true
        customizedView.alpha = 0.75
        customizedView.isUserInteractionEnabled = false
        return customizedView
    }
}

let faceView: UIView = SpecificViewBuilder().build()
