import UIKit
import Foundation

final class ComplicatedObject {
    private var configuration: Data
    
    init(url: URL) {
        let manager = WebManager()
        self.configuration = manager.getData(with: url)
    }
    
    init(object: ComplicatedObject) {
        self.configuration = object.configuration
    }
    
    func clone() -> ComplicatedObject {
        return ComplicatedObject(object: self)
    }
}

final class WebManager {
    func getData(with url: URL) -> Data {
        return Data()
    }
}

let object = ComplicatedObject(url: URL(fileURLWithPath: ""))
let clone = object.clone()
print(object === clone)

/// Another one

private class Author {

    private var id: Int
    private var username: String
    private var pages = [Page]()

    init(id: Int, username: String) {
        self.id = id
        self.username = username
    }

    func add(page: Page) {
        pages.append(page)
    }

    var pagesCount: Int {
        return pages.count
    }
}

private class Page: NSCopying {

    private(set) var title: String
    private(set) var contents: String
    private weak var author: Author?
    private(set) var comments = [Comment]()

    init(title: String, contents: String, author: Author?) {
        self.title = title
        self.contents = contents
        self.author = author
        author?.add(page: self)
    }

    func add(comment: Comment) {
        comments.append(comment)
    }

    /// MARK: - NSCopying

    func copy(with zone: NSZone? = nil) -> Any {
        return Page(title: "Copy of '" + title + "'", contents: contents, author: author)
    }
}

private struct Comment {

    let date = Date()
    let message: String
}

private let author = Author(id: 10, username: "Ivan_83")
private let page = Page(title: "My First Page", contents: "Hello world!", author: author)
guard let anotherPage = page.copy() as? Page else { fatalError() }

page.add(comment: Comment(message: "Keep it up!"))


print("Original title: " + page.title)
print("Copied title: " + anotherPage.title)
print("Count of pages: " + String(author.pagesCount))
