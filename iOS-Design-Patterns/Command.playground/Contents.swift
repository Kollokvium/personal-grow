import UIKit

// When to use:
// - When sequence is important
// - When we need to execute actions in a sequence
// - When we need to undo action ("undo" from Project)
// - When we need to delay execution
// - When we want to serrialize object and send it as a JSON to server
// - When we want an indecation of execution of actions

protocol Command {
    func execute()
}

struct Item: Hashable {
    let name: String?
    let quantity: Int?
}

let colaItem = Item(name: "Cola", quantity: 10)
let removeMockItems = Item(name: "Cola", quantity: 10)

let userBasketItems = [
    Item(name: "Cola", quantity: 10),
    Item(name: "Fanta", quantity: 10),
    Item(name: "Sprite", quantity: 10),
    Item(name: "Yuppi", quantity: 10),
    Item(name: "Baikal", quantity: 10)
]

let serverBasketItems = [
    Item(name: "Cola", quantity: 10),
    Item(name: "Fanta", quantity: 10),
    Item(name: "Sprite", quantity: 10),
    Item(name: "Yuppi", quantity: 10),
    Item(name: "Baikal", quantity: 10),
    Item(name: "Baikal2", quantity: 10)
]

final class Basket {
    var basketItems: Array = [Item]()
    
    func syncBasketItems(userBasket: [Item], serverBasket: [Item]) -> [Item] {
        let userData = Set(userBasket)
        let serverData = Set(serverBasket)
        
        return Array(userData.symmetricDifference(serverData))
    }
    
    func addItem(item: Item) {
        basketItems.append(item)
    }
    
    func deleteItem(item: Item) {
        basketItems.removeAll(where: { $0 == item })
    }
    
    func removeAllItems() {
        basketItems.removeAll()
    }
    
}

class BasketAddItemCommand: Command {
    var basket: Basket
    
    init(basket: Basket) {
        self.basket = basket
    }
    
    func execute() {
        basket.addItem(item: colaItem)
    }
}

class BasketDeleteItemCommand: Command {
    var basket: Basket
    
    init(basket: Basket) {
        self.basket = basket
    }
    
    func execute() {
        basket.deleteItem(item: colaItem)
    }
}

class BasketRemoveAllItemsCommand: Command {
    var basket: Basket
    
    init(basket: Basket) {
        self.basket = basket
    }
    
    func execute() {
        basket.removeAllItems()
    }
}

class BasketSyncItemsCommand: Command {
    var userBasket: Basket
    var serverBasket: Basket
    var resultBasket: Basket?
    
    init(userBasket: Basket, serverBasket: Basket) {
        self.userBasket = userBasket
        self.serverBasket = serverBasket
    }
    
    func execute() {
        resultBasket?.syncBasketItems(userBasket: userBasketItems, serverBasket: serverBasketItems)
    }
    
    func showMergetBasket() -> Basket? {
        return resultBasket
    }
}

class BasketManager {
    var command: [Command] = []
    
    func start() {
        command.forEach { $0.execute() }
    }
    
    func undoLast() {
        command.popLast()
    }
}

// General

let someRandomBasket = Basket()
print("Initial User Basket State", someRandomBasket.basketItems)

let addItemComand = BasketAddItemCommand(basket: someRandomBasket)
let deleteItemComand = BasketDeleteItemCommand(basket: someRandomBasket)
let removeAllItemComand = BasketRemoveAllItemsCommand(basket: someRandomBasket)

// Command Manager
let basketManager = BasketManager()

basketManager.command.append(addItemComand)
basketManager.start()
print("State after receiving ADD Command", someRandomBasket.basketItems)

basketManager.command.append(deleteItemComand)
basketManager.start()
print("State after receiving DELETE Command", someRandomBasket.basketItems)

// Case Specific

let anotherBasketManager = BasketManager()

let userGroceryBasket = Basket()
userGroceryBasket.basketItems = userBasketItems

let serverGroceryBasket = Basket()
serverGroceryBasket.basketItems = serverBasketItems

let removeAllSpecificCommand = BasketRemoveAllItemsCommand(basket: userGroceryBasket)
let fullUserBasketCommand = BasketAddItemCommand(basket: userGroceryBasket)
let fullServerBasketCommand = BasketAddItemCommand(basket: serverGroceryBasket)
let mergeBacketCommand = BasketSyncItemsCommand(userBasket: userGroceryBasket, serverBasket: serverGroceryBasket)

anotherBasketManager.command.append(removeAllSpecificCommand)
anotherBasketManager.start()
print("Basket State after REMOVE ALL ", userGroceryBasket.basketItems)

anotherBasketManager.command.append(fullUserBasketCommand)
anotherBasketManager.command.append(fullServerBasketCommand)
print("USER BASKET:", userGroceryBasket.basketItems)
print("SERVER BASKET:", serverGroceryBasket.basketItems)

anotherBasketManager.command.append(mergeBacketCommand)
anotherBasketManager.start()
print("BASKET after MERGE COMMAND:", userGroceryBasket.basketItems.hashValue)
print("BASKET after MERGE COMMAND:", serverGroceryBasket.basketItems.hashValue)

let result = mergeBacketCommand.resultBasket
print("mergeBacketCommand.resultBasket:", mergeBacketCommand.resultBasket as Any)
print("mergeBacketCommand.resultBasket:", result as Any)

