import UIKit
import PlaygroundSupport

PlaygroundPage.current.needsIndefiniteExecution = true

let queue = DispatchQueue(label: "Semaphore Study", attributes: .concurrent)
// DispatchSemaphore(value: 2) the amount of threads that are going to pass
let semaphore = DispatchSemaphore(value: 1) // In order to see diff change value

queue.async {
    semaphore.wait() // we are going to **Decrement** by 1 the amount of threads that are going to pass
    sleep(3)
    print(" have execution 1 ")
    semaphore.signal() // we are going to **Increment** by 1 the amount of threads that are going to pass
}

queue.async {
    semaphore.wait()
    sleep(3)
    print(" have execution 2 ")
    semaphore.signal()
}

queue.async {
    semaphore.wait()
    sleep(3)
    print(" have execution 3 ")
    semaphore.signal()
}

class Counter {
    private let semaphoreCounter = DispatchSemaphore(value: 1)
    private var counter = [Int]()
    
    private func startJob(_ workers: Int) {
        semaphoreCounter.wait()
        counter.append(workers)
        print("Work items: \(counter.count)")
        sleep(1)
        semaphoreCounter.signal()
    }
    
    public func startAllThreads() {
        DispatchQueue.global().async {
            print("self.startJob(100)")
            self.startJob(100)
        }
        
        DispatchQueue.global().async {
            print("self.startJob(101)")
            self.startJob(101)
        }
        
        DispatchQueue.global().async {
            print("self.startJob(102)")
            self.startJob(102)
        }
        
        DispatchQueue.global().async {
            print("self.startJob(103)")
            self.startJob(103)
        }
    }
}

let counter = Counter()
counter.startAllThreads()
