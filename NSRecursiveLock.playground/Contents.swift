import UIKit

// We can acquire the same resource by the same thread multiple times without a deadlock.
// For example in a recursion.

// In order to check diff change NSRecursiveLock --> NSLock
let recursiveLock = NSRecursiveLock()

class RecursiveThread: Thread {
    
    override func main() {
        recursiveLock.lock()
        print("thread is get locked")
        checker()
        defer { recursiveLock.unlock() }
        print("exit main func")
    }
    
    func checker() {
        recursiveLock.lock()
        print("thread is get locked")
        defer { recursiveLock.unlock() }
        print("exit checker func")
    }
}

let thread = RecursiveThread()
thread.start()
