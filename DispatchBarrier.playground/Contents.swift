import UIKit
import PlaygroundSupport

PlaygroundPage.current.needsIndefiniteExecution = true

// ******** Problem ******** //

var array = [Int]()

// Race condition
// Unexpected value, count, execution
DispatchQueue.concurrentPerform(iterations: 10) { count in
    array.append(count)
}

print(array)
print(array.count)

// ******** Problem Solver ******** //

class CellViewModelGenerator<T> {
    private lazy var cellViewModels = [T]()
    private lazy var queue = DispatchQueue(label: "Safe Operation", attributes: .concurrent)
    
    public var viewModelValue: [T] {
        var result = [T]()
        queue.sync {
            result = self.cellViewModels
        }
        
        return result
    }
    
    public func append(_ viewModel: T) {
        queue.async(flags: .barrier) {
            self.cellViewModels.append(viewModel)
        }
    }
}

struct CellData {
    let name: String
    let money: Int
}

var cellViewModels = CellViewModelGenerator<Int>()
DispatchQueue.concurrentPerform(iterations: 5) { (count) in
//    cellViewModels.append(CellData(name: "Some Guy", money: count))
    cellViewModels.append(count)
}

// Persistant amount and stable result
print("cellViewModels:", cellViewModels.viewModelValue)
print("cellViewModels Count:", cellViewModels.viewModelValue.count)
