import UIKit

/// A mutex is a type of semaphore that grants access to only one thread at a time.
/// If a mutex is in use and another thread tries to acquire it,
/// that thread blocks until the mutex is released by its original holder.

// C variation of Mutex functionality
// Benchmark x20 time faster than any wraper
class SafeZone {
    private var mutex = pthread_mutex_t()
    
    init() {
        pthread_mutex_init(&mutex, nil)
    }
    
    func havyCalculation(completion: () -> ()) {
        defer { pthread_mutex_unlock(&mutex) }
        
        pthread_mutex_lock(&mutex)
        completion()
    }
}

// Objective-C variation of Mutex functionality
class ObjectiveSafeZone {
    private let lockMutex = NSLock()
    
    func havyCalculation(completion: () -> ()) {
        lockMutex.lock()
        completion()
        lockMutex.unlock()
    }
}

// Data
var someData = [Int]()
let filler = [0...100]

// C Init
let safeReadWrite = SafeZone()

safeReadWrite.havyCalculation {
    let data = filler.flatMap { $0 }
    data.map { someData.append(Int($0)) }
    
    print(someData)
}

someData.append(101)
print(someData.last as Any)
someData.append(102)
print(someData.last as Any)
someData.append(103)
print(someData.last as Any)

// Obj-C Init
let safeDataManagment = ObjectiveSafeZone()
safeDataManagment.havyCalculation {
    let data = filler.flatMap { $0 }
    data.map { someData.removeAll(); someData.append(Int($0)) }
    
    print(someData)
}

