import UIKit
import PlaygroundSupport

class QueueClass {
    private let serialQueue = DispatchQueue(label: "Just Serial Queue")
    private let cuncurrentQueue = DispatchQueue(label: "Just Cuncurrent Queue", attributes: .concurrent)
}

class SystemQueueClass {
    private let globalQueue = DispatchQueue.global()
    private let mainQueue = DispatchQueue.main
}

class DebugViewController: UIViewController {
    
    let psuherButton = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        title = "My Practice"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        showButton()
    }
    
    func showButton() {
        view.addSubview(psuherButton)
        psuherButton.frame = CGRect(x: 0, y: 0, width: 100, height: 40)
        psuherButton.center = view.center
        psuherButton.setTitle("Just do it", for: .normal)
        psuherButton.backgroundColor = .green
        psuherButton.addTarget(self,
                               action: #selector(buttonAction),
                               for: .touchUpInside)
    }
    
    @objc func buttonAction() {
        let targetViewController = TargetViewController()
        navigationController?.pushViewController(targetViewController, animated: true)
    }
}

class TargetViewController: UIViewController {
    
    lazy var targetImage = UIImageView()
    lazy var imageSource: URL = URL(string: "https://klike.net/uploads/posts/2018-06/1528720172_1.jpg")!
    lazy var dataImage = try? Data(contentsOf: imageSource)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        title = "TargetViewController"
        
        load()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        showImage()
    }
    
    func showImage() {
        view.addSubview(targetImage)
        targetImage.frame = CGRect(x: 0, y: 0, width: 200, height: 200)
        targetImage.center = view.center
        targetImage.contentMode = .scaleAspectFit
        targetImage.clipsToBounds = true
        targetImage.layer.cornerRadius = 15
        targetImage.layer.masksToBounds = true
    }
    
    func load() {
        DispatchQueue.global(qos: .utility).async {
            if let dataImage = self.dataImage {
                DispatchQueue.main.async {
                    self.targetImage.image = UIImage(data: dataImage)
                }
            }
        }
    }
}

let viewController = DebugViewController()
let navigationViewController = UINavigationController(rootViewController: viewController)

PlaygroundPage.current.liveView = navigationViewController
